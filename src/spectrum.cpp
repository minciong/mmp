#include <cstdlib>
#include <GL/glu.h>
#include <iostream>
#include <QApplication>
#include <QtWidgets>

#include <QTimer>
#include <cmath>
#include "spectrum.h"
#include <ctime>
#include <QOpenGLWidget>

#include <iostream>
using namespace std;

spectrum::spectrum(QWidget *parent):
    /** Widget initialized.
     */
    QOpenGLWidget(parent){
    srand(time(NULL));
    initializeGL();
    setFocusPolicy(Qt::StrongFocus);
    m_timer = new QTimer(this);
    connect(m_timer, SIGNAL(timeout()),this,SLOT(update()));
    m_timer->start(25);
}

void spectrum::update() {
    paintGL();
}

void spectrum::paintGL(){
    adjust();
    display();
}

void spectrum::initializeGL(){
    glEnable(GL_DEPTH_TEST);
    glClearColor(0, 0, 0, 0);
}

void spectrum::stopFeed(){
    /** When music is stopped, timer stops.
     */
    a_timer->stop();
    c_timer = new QTimer(this);
    connect(c_timer, SIGNAL(timeout()), this, SLOT(internalWindDown()));
    c_timer->start(100); //Determines how fast the bar drops
}

void spectrum::internalWindDown(){
    /** Bars lower over time when music stops.
     */
    winded_down = 1; //if values in array are not zero, make it zero using boolean variable.
    for (int i = 0; i < 10; i++){
            if (sizes[i] != 0){
                sizes[i] = 0;
                winded_down = 0;
            }
    }
    if (winded_down == 1){ //if statement prevents spectrum from breaking if boolean is still true.
        c_timer->stop();
    }
}

void spectrum::startFeed(){
    /** When music begins, timer begins for spectrum to move by calling slot function randomize
     */
    a_timer = new QTimer(this);
    connect(a_timer, SIGNAL(timeout()), this, SLOT(randomize()));
    a_timer->start(100);
}

void spectrum::pauseFeed(){
    /** Same as stopFeed, but keeps spectrum value in place.
     */
    a_timer->stop();
}

void spectrum::resizeGL()
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void spectrum::randomize(){
    /** Feeds random values into each bar of the spectrum.
     */
    for (int i = 0; i < 10; i++){ //random values less than 1 to prevent bar height from exceeding window.
        sizes[i] = (double)(rand()%50)/25-0.3;

    }
    if (firstrun){ //Runs once using boolean variable. Replaces oldsize[i] with sizes[i] to prevent bar height from getting stuck.
        firstrun = 0;
        for (int i = 0; i < 10; i++){
            oldsizes[i] = sizes[i];
        }
    }
}

void spectrum::adjust(){
    /** Compares the arrays and gives old array new array sizes if oldsize is greater. Size is slowly decrements values.
     */
    for (int i = 0; i < 10; i++){
        if (oldsizes[i] > sizes[i])
            sizes[i] = oldsizes[i]-0.1;
            oldsizes[i] = sizes[i];
    }
}

void spectrum::drawbox(int i){
    /** Draws a square by using vertex using 4 coordinates, bottom left, bottom right, top left, and top right with top values having changing heights according to sizes.
     */
    glBegin(GL_POLYGON);
    glVertex2f(-0.1,-0.1);
    glVertex2f(-0.1,sizes[i]);
    glVertex2f(0.1,sizes[i]);
    glVertex2f(0.1,-0.1);
    glEnd();
    }

void spectrum::display()
{
    /** Translates and displays the spectrum
     */
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    glTranslatef(-0.9,-0.9,0); //Square moves to bottom left of the window
    glColor3f(1, 0, 0);
    for (int i=0;i<10;i+=1){ //Multiplies square 9 times and translate them to the right each time.
        glPushMatrix();
        glTranslatef(i*0.2,0,0);
        glColor3f(sizes[i], i*0.1, 0); //Changes color as bar height changes according to sizes.
            drawbox(i);
            glPopMatrix();
    }

}
