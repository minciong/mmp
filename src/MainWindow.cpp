// ======================================================================
// MainWindow.cpp - Main Window widget
//
// Lazy Asian Development Syndicate - Musically Musical Player
// ======================================================================

//!  MainWindow Class
/*!
  Contains the bulk of the mediaplayer functionality.
*/

#include <QtWidgets>
#include <iomanip>
#include <stdio.h>
#include <QMediaMetaData>
#include <fileref.h>
#include <tag.h>
#include <tpropertymap.h>


//Coverart Things
#include <mpegfile.h>
#include <attachedpictureframe.h>
#include <id3v2tag.h>
#include <mp4tag.h>
#include <mp4file.h>
#include <flacfile.h>
#include <flacpicture.h>
#include <flacmetadatablock.h>
#include<id3v2frame.h>

//Main Header
#include "MainWindow.h"

using namespace std;

enum {TITLE, TRACK, TIME, ARTIST, ALBUM, GENRE, PATH};
const int COLS = PATH+1;

bool caseInsensitive(const QString &s1, const QString &s2)
{
    return s1.toLower() < s2.toLower();
}
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// MainWindow::MainWindow:
//
// Constructor. Initialize user-interface elements.
//
MainWindow::MainWindow	(QString program)
       : m_directory(".")
{
    /**
      Constructor. Sets up the main window with actions, menus, widgets,and layouts.
      Also sets up the default size of the window on startup.
      */
    // setup GUI with actions, menus, widgets, and layouts
    createActions();	// create actions for each menu item
    createMenus  ();	// create menus and associate actions
    createWidgets();	// create window widgets
    createLayouts();	// create widget layouts

    // populate the list widgets with music library data
    initLists();		// init list widgets

    // set main window titlebar
    QString title = "Musically Musical Player";
    setWindowTitle(title);

    // set central widget and default size
    mediaplayer = new QMediaPlayer;
    playlist = new QMediaPlaylist;
    mediaplayer->setPlaylist(playlist);


    setCentralWidget(m_mainWidget);
    setMinimumSize(800, 400);
    resize(800, 600);
}



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// MainWindow::~MainWindow:
//
//
MainWindow::~MainWindow() {}



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// MainWindow::createActions:
//
void
MainWindow::createActions()
{
    /** Create actions to associate with menu and toolbar selection.
     */
    m_loadAction = new QAction("&Load Music Folder", this);
    m_loadAction->setShortcut(tr("Ctrl+L"));
    connect(m_loadAction, SIGNAL(triggered()), this, SLOT(s_load()));

    m_quitAction = new QAction("&Quit", this);
    m_quitAction->setShortcut(tr("Ctrl+Q"));
    connect(m_quitAction, SIGNAL(triggered()), this, SLOT(close()));

    m_aboutAction = new QAction("&About", this);
    m_aboutAction->setShortcut(tr("Ctrl+A"));
    connect(m_aboutAction, SIGNAL(triggered()), this, SLOT(s_about()));

    m_toggleDockAction = new QAction("&Customize Layout", this);
    m_toggleDockAction->setShortcut(tr("Ctrl+C"));
    connect(m_toggleDockAction, SIGNAL(triggered()), this, SLOT(s_toggleDocks()));
}



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// MainWindow::createMenus:
//
void
MainWindow::createMenus()
{
    /** Create menus and install in menubar.
     */
    m_fileMenu = menuBar()->addMenu("&File");
    m_fileMenu->addAction(m_loadAction);
    m_fileMenu->addAction(m_quitAction);

    m_widgetMenu = menuBar()->addMenu("&Widgets");
    m_widgetMenu->addAction(m_toggleDockAction);

    m_helpMenu = menuBar()->addMenu("&Help");
    m_helpMenu->addAction(m_aboutAction);
}



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// MainWindow::createWidgets:
//
//
void
MainWindow::createWidgets()
{
    /** Create widgets to put in the layout.
     */

    //Init mainwidget for the base layout
    m_mainWidget = new QWidget;

    //Make those control buttons
    for (int i = 0; i < ENUM_CONTROLS_END; i++) {
        player_controls[i] = new QPushButton;
        player_controls[i]->setMinimumHeight(24);
        //player_controls[i]->setDisabled(true);
    }
    m_progress_slider = new QSlider(Qt::Horizontal, this);
    m_progress_slider->setFixedHeight(24);
    m_progress_slider->setRange(0, 100);
    m_progress_slider->setDisabled(true);

    //Audio Control
    for (int i = 0; i < ENUM_AUD_CONTROLS_END; i++) {
            m_aud_controls[i] = new QPushButton;
            m_aud_controls[i]->setMinimumHeight(24);
    }
    m_volume_slider = new QSlider(Qt::Horizontal, this);
    m_volume_slider->setRange(0, 100);
    m_volume_slider->setMinimumHeight(24);
    m_volume_slider->setMaximumWidth(64);
    m_volume_slider->setValue(100);

    //style the controls
    player_controls[PREVIOUS]->setIcon(QApplication::style()->standardIcon(QStyle::SP_MediaSkipBackward));
    player_controls[PLAY]->setIcon(QApplication::style()->standardIcon(QStyle::SP_MediaPlay));
    player_controls[STOP]->setIcon(QApplication::style()->standardIcon(QStyle::SP_MediaStop));
    player_controls[NEXT]->setIcon(QApplication::style()->standardIcon(QStyle::SP_MediaSkipForward));


    player_controls[SHUFFLE]->setIcon(QApplication::style()->standardIcon(QStyle::SP_BrowserReload));

    m_aud_controls[MUTE]->setIcon(QApplication::style()->standardIcon((QStyle::SP_MediaVolume)));

    //Add a dropdown box for the playback speed
    m_playback_speed = new QComboBox;
    m_playback_speed->setMaximumHeight(25);
    m_playback_speed->addItem(tr("0.5x"));
    m_playback_speed->addItem(tr("1.0x"));
    m_playback_speed->addItem(tr("1.25x"));
    m_playback_speed->addItem(tr("1.5x"));
    m_playback_speed->addItem(tr("1.75x"));
    m_playback_speed->addItem(tr("2.0x"));
    m_playback_speed->setCurrentIndex(1);

    //Connect controls to functions
    QObject::connect(player_controls[PREVIOUS], SIGNAL(clicked()), this, SLOT(s_previous()));
    QObject::connect(player_controls[PLAY], SIGNAL(clicked()), this, SLOT(s_play()));
    QObject::connect(player_controls[STOP], SIGNAL(clicked()), this, SLOT(s_stop()));
    QObject::connect(player_controls[NEXT], SIGNAL(clicked()), this, SLOT(s_next()));

    QObject::connect(m_aud_controls[MUTE], SIGNAL(clicked()), this, SLOT(s_mute()));
    connect(m_progress_slider, SIGNAL(sliderMoved(int)),this, SLOT(s_progress_slider_change(int)));

    QObject::connect(player_controls[SHUFFLE], SIGNAL(clicked()), this, SLOT(s_playmode()));

    QObject::connect(m_volume_slider, SIGNAL(sliderMoved(int)), this, SLOT(changeVolume(int)));

    connect(m_playback_speed, SIGNAL(currentIndexChanged(int)), this, SLOT(s_playback_speed(int)));

    //Information
    m_playtime = new QLabel;
    m_playtime->setFixedHeight(24);
    m_playtime->setText("Playback Stopped");
    m_playtime->setFrameStyle(QFrame::Box | QFrame::Sunken);

    // init labelArt widget
        m_labelArt = new QLabel;
        m_labelArt->setAlignment(Qt::AlignCenter);

    // initialize labels on top of the filters
    for(int i=0; i<3; i++) {
        // make label widget with centered text and sunken panels
        m_label[i] = new QLabel;
        m_label[i]->setAlignment(Qt::AlignCenter);
        m_label[i]->setFrameStyle(QFrame::Panel | QFrame::Sunken);
    }

    // initialize label text
    m_label[0]->setText("<b>Genre<\b>" );
    m_label[1]->setText("<b>Artist<\b>");
    m_label[2]->setText("<b>Album<\b>");

    // initialize list widgets: genre, artist, album
    for(int i=0; i<3; i++)
        m_panel[i] = new QListWidget;

    // initialize table widget: complete song data
    m_table = new QTableWidget(0, COLS);
    QHeaderView *header = new QHeaderView(Qt::Horizontal,m_table);
    header->setSectionResizeMode(QHeaderView::Stretch);
    m_table->setHorizontalHeader(header);
    m_table->setHorizontalHeaderLabels(QStringList() <<
        "Name" << "Track" << "Time" << "Artist" << "Album" << "Genre");
    m_table->setAlternatingRowColors(1);
        m_table->setShowGrid(1);
        m_table->setEditTriggers (QAbstractItemView::NoEditTriggers);
        m_table->setSelectionBehavior(QAbstractItemView::SelectRows);
   m_table->setColumnHidden(PATH, 1);

    // init signal/slot connections
    connect(m_panel[0],	SIGNAL(itemClicked(QListWidgetItem*)),
        this,		  SLOT(s_panel1   (QListWidgetItem*)));
        connect(m_panel[1],	SIGNAL(itemClicked(QListWidgetItem*)),
        this,		  SLOT(s_panel2   (QListWidgetItem*)));
        connect(m_panel[2],	SIGNAL(itemClicked(QListWidgetItem*)),
        this,		  SLOT(s_panel3   (QListWidgetItem*)));
        connect(m_table, SIGNAL(itemDoubleClicked(QTableWidgetItem*)),
            this, SLOT(s_ml_add_play(QTableWidgetItem*)));

        connect(m_table, SIGNAL(itemClicked(QTableWidgetItem*)), this, SLOT(s_extract_art(QTableWidgetItem*))); //View art on single click

        m_table->horizontalHeader()->setSectionsClickable(true);
        connect(m_table->horizontalHeader(), SIGNAL(sectionClicked(int)), this, SLOT(s_header_adjust(int)));



     //Playlist table
        m_playlist_view = new QTableWidget(0, 3);
        QHeaderView *header_playlist = new QHeaderView(Qt::Horizontal,m_playlist_view);
        m_playlist_view->setHorizontalHeader(header_playlist);
        m_playlist_view->setHorizontalHeaderLabels(QStringList() << "" << "Time" << "Name");
        m_playlist_view->setAlternatingRowColors(1);
            m_playlist_view->setShowGrid(1);
            m_playlist_view->setEditTriggers (QAbstractItemView::NoEditTriggers);
            m_playlist_view->setSelectionBehavior(QAbstractItemView::SelectRows);

            m_playlist_view->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Fixed);
            m_playlist_view->horizontalHeader()->setStretchLastSection(true);
            m_playlist_view->setColumnWidth(0, 20);
            m_playlist_view->setColumnWidth(1, 50);
            m_playlist_view->horizontalHeader()->setSectionResizeMode(1,QHeaderView::Interactive);

        // init signal/slot connections
            connect(m_playlist_view, SIGNAL(itemDoubleClicked(QTableWidgetItem*)), this, SLOT(s_playlist_spec(QTableWidgetItem*)));
            load_default_album_art();

            setTabPosition(Qt::TopDockWidgetArea, QTabWidget::North);

      //Coverflow things
            coverflow_dock =  new QDockWidget(tr("Coverflow"), this);

            coverflow = new Coverflow(coverflow_dock);
            m_widgetMenu->addAction(coverflow_dock->toggleViewAction());

            coverflow_dock->setWidget(coverflow);
            coverflow_dock->setMinimumHeight(200);

      //Spectrum things
            spectrum_dock = new QDockWidget(tr("Spectrum Visualizer"), this);

            spectrum_widget = new spectrum(spectrum_dock);
            m_widgetMenu->addAction(spectrum_dock->toggleViewAction());

            spectrum_dock->setWidget(spectrum_widget);
            spectrum_dock->setMinimumHeight(200);
}



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// MainWindow::createLayouts:
//
//
void MainWindow::createLayouts()
{
    /** Create layouts for widgets.
            */
    // create a 2-row, 3-column grid layout, where row0 and
    // row1 consist of labels and list widgets, respectively.
    QWidget	    *widget = new QWidget(m_mainWidget);
    QGridLayout *grid   = new QGridLayout(widget);
    grid->addWidget(m_label[0], 0, 0);
    grid->addWidget(m_label[1], 0, 1);
    grid->addWidget(m_label[2], 0, 2);
    grid->addWidget(m_panel[0], 1, 0);
    grid->addWidget(m_panel[1], 1, 1);
    grid->addWidget(m_panel[2], 1, 2);

    // add widgets to the splitters

    //Places m_panel into a dock
    m_panel_dock = new QDockWidget(tr("Filters"));
    m_panel_dock->setWidget(widget);
    m_widgetMenu->addAction(m_panel_dock->toggleViewAction());


    //Init Controls
    QWidget *p_controls = new QWidget(this);
    QHBoxLayout *layout_controls = new QHBoxLayout(p_controls); //Horizontal controls, derp
    layout_controls->setMargin(0); //One of these has to work
    layout_controls->setSpacing(0);
    layout_controls->setContentsMargins(0,0,0,0);
    layout_controls->setAlignment(Qt::AlignCenter);
    for (int i = 0; i < ENUM_CONTROLS_END; i++) {
        layout_controls->addWidget(player_controls[i]);
    }
    layout_controls->addWidget(m_progress_slider);
    layout_controls->addWidget(m_playtime);
    layout_controls->addWidget(m_playback_speed);
    layout_controls->addWidget(m_aud_controls[MUTE]);
    layout_controls->addWidget(m_volume_slider);


    //Player controls dock
    p_controls_dock = new QDockWidget(tr("Controls"), this);
    p_controls_dock->setWidget(p_controls);
    p_controls_dock->setFeatures(QDockWidget::DockWidgetFloatable | QDockWidget::DockWidgetMovable);



    //Put the m_table widget in a dock
    m_table_dock = new QDockWidget(tr("Library"));
    m_table_dock->setWidget(m_table);

    //Add the m_table dock widget toggle action to the Widget menu bar
    m_widgetMenu->addAction(m_table_dock->toggleViewAction());

    //Put the playlist in a dock
    m_playlist_dock = new QDockWidget(tr("Playlist"), this);
    m_playlist_dock->setWidget(m_playlist_view);

    //Add the playlist dock widget toggle action to the Widget menu bar
    m_widgetMenu->addAction(m_playlist_dock->toggleViewAction());

    //Put the album art widget into a dock
    m_labelArt_dock = new QDockWidget(tr("Album Art"));
    m_labelArt_dock->setWidget(m_labelArt);

    //Add the album art dock widget toggle action to the Widget menu bar
    m_widgetMenu->addAction(m_labelArt_dock->toggleViewAction());

    //Add all of the dock widgets into the main widget, emulating the original layout
    addDockWidget(Qt::RightDockWidgetArea, m_panel_dock);
    addDockWidget(Qt::RightDockWidgetArea, m_table_dock);
    addDockWidget(Qt::LeftDockWidgetArea, m_playlist_dock);
    addDockWidget(Qt::LeftDockWidgetArea, m_labelArt_dock);
    addDockWidget(Qt::BottomDockWidgetArea, p_controls_dock);
    addDockWidget(Qt::TopDockWidgetArea, spectrum_dock);
    addDockWidget(Qt::TopDockWidgetArea, coverflow_dock);
    tabifyDockWidget(spectrum_dock,coverflow_dock);


    s_toggleDocks(); //By default, let's hide the title bars to make everything look cleaner
}



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// MainWindow::initLists:
//
//
void MainWindow::initLists(){
    /** Populate lists with data (first time).
     */
    // error checking
    if(m_listSongs.isEmpty()) return;

    for (int i = 0; i < 3; i++){
        m_panel[i]->clear();
    }
    m_table->clearContents();

    // create separate lists for genres, artists, and albums
    for(int i=0; i<m_listSongs.size(); i++)
        m_listGenre  << m_listSongs[i][GENRE];
        // do same for m_listArtist and m_listAlbum using ARTIST and ALBUM constants

    for(int i=0; i<m_listSongs.size(); i++)
        m_listArtist  << m_listSongs[i][ARTIST];

    for(int i=0; i<m_listSongs.size(); i++)
        m_listAlbum  << m_listSongs[i][ALBUM];


    // sort each list
    qStableSort(m_listGenre .begin(), m_listGenre .end(), caseInsensitive);
    qStableSort(m_listArtist.begin(), m_listArtist.end(), caseInsensitive);
    qStableSort(m_listAlbum .begin(), m_listAlbum .end(), caseInsensitive);

    m_listGenre.prepend("#No Filter#");

    // add each list to list widgets, filtering out repeated strings
    for(int i=0; i<m_listGenre.size(); i+=m_listGenre.count(m_listGenre[i]))
        m_panel[0]->addItem(m_listGenre [i]);

    // do same for m_listArtist and m_listAlbum to populate m_panel[1] and m_panel[2]
    for(int i=0; i<m_listArtist.size(); i+=m_listArtist.count(m_listArtist[i]))
        m_panel[1]->addItem(m_listArtist [i]);

    for(int i=0; i<m_listAlbum.size(); i+=m_listAlbum.count(m_listAlbum[i]))
        m_panel[2]->addItem(m_listAlbum [i]);


    // copy data to table widget
    QTableWidgetItem *item[COLS];
    for(int i=0; i<m_listSongs.size(); i++) {
        if (m_table->rowCount() < m_listSongs.size())
            m_table->insertRow(i);
        for(int j=0; j<COLS; j++) {
            item[j] = new QTableWidgetItem;
            item[j]->setText(m_listSongs[i][j]);
            item[j]->setTextAlignment(Qt::AlignCenter);
            m_table->setItem(i, j, item[j]);
        }
    }
}



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// MainWindow::redrawLists:
//
//
void MainWindow::redrawLists(QListWidgetItem *listItem, int x)
{
    /** Re-populate lists with data matching item's text in field x.
    */
    m_table->setSortingEnabled(false);
    m_table->setRowCount(0);
    // copy data to table widget
    for(int i=0,row=0; i<m_listSongs.size(); i++) {
        // skip rows whose field doesn't match text
        if(m_listSongs[i][x] != listItem->text()) continue;

        m_table->insertRow(row);
        QTableWidgetItem *item[COLS];
        for(int j=0; j<COLS; j++) {
            item[j] = new QTableWidgetItem;
            item[j]->setText(m_listSongs[i][j]);
            item[j]->setTextAlignment(Qt::AlignCenter);
            m_table->setItem(row, j, item[j]);
        }
        // increment table row index (row <= i)
        row++;
    }
}



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// MainWindow::traverseDirs:
//
//
void
MainWindow::traverseDirs(QString path){
    /** Traverse all subdirectories and collect filenames into m_listSongs.
     */
    QString		key, val;
    QStringList	list;

    // init listDirs with subdirectories of path
    QDir dir(path);
    dir.setFilter(QDir::AllDirs | QDir::NoDotAndDotDot);
    QFileInfoList listDirs = dir.entryInfoList();

    // init listFiles with all *.mp3 files in path
    QDir files(path);
    files.setFilter(QDir::Files);
    files.setNameFilters(QStringList() << "*.mp3" << "*.flac" << "*.mp4" << "*.m4a" << "*.wav");
    QFileInfoList listFiles = files.entryInfoList();

    for(int i=0; i < listFiles.size(); i++) {
        // adjust progress dialog to current settings (optional for now)
        // init list with default values: ""
        for(int j=0; j<=COLS; j++)
            list.insert(j, "");

        // store file pathname into 0th position in list
        QFileInfo fileInfo = listFiles.at(i);
        list.replace(PATH, fileInfo.filePath());

        // convert it from QString to Ascii and store in source
        // Note: this uses audiere. Replace with function from Qt5 multimedia module
        //SampleSourcePtr source = OpenSampleSource(fileInfo.filePath().toLatin1());

        //This takes them tags, make this another function later
        TagLib::FileRef source(QFile::encodeName(fileInfo.filePath()).constData());
        if(!source.isNull() && source.tag() ) {
            TagLib::Tag *tag = source.tag();
            if(tag->genre() != "") list.replace(GENRE, TStringToQString(tag->genre()));
            else list.replace(GENRE, "Unknown");
            if(tag->artist() != "") list.replace(ARTIST, TStringToQString(tag->artist()));
            else list.replace(ARTIST, "Unknown");
            if(tag->album() != "") list.replace(ALBUM, TStringToQString(tag->album()));
            else list.replace(ALBUM, "Unknown");
            if(tag->title() != "") list.replace(TITLE, TStringToQString(tag->title()));
            else list.replace(TITLE, fileInfo.fileName());
            //duplicate this a lot for everything
            QString temp_track = QString("%1").arg(tag->track());
            if(temp_track != "0") list.replace(TRACK, temp_track); //Change this to just print
            else list.replace(TRACK, "Unknown");


            if (source.audioProperties()){
                TagLib::AudioProperties *prop = source.audioProperties();
                int seconds = prop->length() % 60;
                int minutes = (prop->length())/60; //-seconds werehere
                QString temp_time;
                if (seconds < 10)
                    temp_time = QString("%1:0%2").arg(minutes).arg(seconds);
                else temp_time = QString("%1:%2").arg(minutes).arg(seconds);
                list.replace(TIME, temp_time);
            }

        }
        // End getting tags

        // append list (song data) into songlist m_listSongs;
        // uninitialized fields are empty strings
        m_listSongs << list;
    }

    // base case: no more subdirectories
    if(listDirs.size() == 0) return;

    // recursively descend through all subdirectories
    for(int i=0; i<listDirs.size(); i++) {
        QFileInfo fileInfo = listDirs.at(i);
        traverseDirs( fileInfo.filePath() );
    }

    return;
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// MainWindow::s_load:
//
//
void
MainWindow::s_load()
{
    /** Slot function for File|Load
     */
    // open a file dialog box
    QFileDialog *fd = new QFileDialog;

    fd->setFileMode(QFileDialog::Directory);
    QString s = fd->getExistingDirectory(0, "Select Folder", m_directory,
             QFileDialog::ShowDirsOnly |
             QFileDialog::DontResolveSymlinks);

    // check if cancel was selected
    if(s == NULL) return;

    // copy full pathname of selected directory into m_directory
    m_directory = s;

    traverseDirs(m_directory);
    initLists();
}



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// MainWindow::s_panel1:
//
//
void
MainWindow::s_panel1(QListWidgetItem *item)
{
    /** Slot function to adjust data if an item in panel1 (genre) is selected.
     */
    if(item->text() == "#No Filter#") { //Reimplement this later
        initLists();
        return;
    }

    // clear lists
    m_panel[1] ->clear();
    m_panel[2] ->clear();
    m_listArtist.clear();
    m_listAlbum .clear();

    // collect list of artists and albums
    for(int i=0; i<m_listSongs.size(); i++){
        if (m_listSongs[i][GENRE] == item->text()){
            m_listArtist  << m_listSongs[i][ARTIST];
            m_listAlbum  << m_listSongs[i][ALBUM];
        }
    }

    // sort remaining two panels for artists and albums
    qStableSort(m_listArtist.begin(), m_listArtist.end(), caseInsensitive);
    qStableSort(m_listAlbum.begin(), m_listAlbum.end(), caseInsensitive);


    for(int i=0; i<m_listArtist.size(); i+=m_listArtist.count(m_listArtist[i])){
        m_panel[1]->addItem(m_listArtist[i]);
    }

    for(int i=0; i<m_listAlbum.size(); i+=m_listAlbum.count(m_listAlbum[i])){
        m_panel[2]->addItem(m_listAlbum[i]);
    }

    redrawLists(item, GENRE);

}



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// MainWindow::s_panel2:
//
//
void
MainWindow::s_panel2(QListWidgetItem *item)
{
    /** Slot function to adjust data if an item in panel2 (artist) is selected.
     */
    // clear lists
    m_panel[2]->clear();
    m_listAlbum.clear();

    // collect list of albums
    for(int i=0; i<m_listSongs.size(); i++) {
        if(m_listSongs[i][ARTIST] == item->text())
            m_listAlbum << m_listSongs[i][ALBUM];
    }

    // sort remaining panel for albums
    qStableSort(m_listAlbum.begin(), m_listAlbum.end(), caseInsensitive);

    // add items to panel; skip over non-unique entries
    for(int i=0; i<m_listAlbum.size(); i+=m_listAlbum.count(m_listAlbum[i]))
        m_panel[2]->addItem(m_listAlbum[i]);

    redrawLists(item, ARTIST);
}



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// MainWindow::s_panel3:
//
//
void
MainWindow::s_panel3(QListWidgetItem *item)
{
    /** Slot function to adjust data if an item in panel3 (album) is selected.
     */
    redrawLists(item, ALBUM);
}


// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// MainWindow::s_head_adjust:
//
//
void MainWindow::s_header_adjust(int column){
    /** Enables the main song table list widget to sort based on the selected header
     */
    m_table->setSortingEnabled(true);
    if (m_lastsort != column){
        m_lastsort = column;
        m_sortorder = 0;
        m_table->sortByColumn(column, Qt::AscendingOrder);
    }
    else if (m_lastsort == column){
        if (m_sortorder == 0){
            m_table->sortByColumn(column, Qt::DescendingOrder);
            m_sortorder = 1;
        }
        else {
            m_table->sortByColumn(column, Qt::AscendingOrder);
            m_sortorder = 0;
        }
    }
}


// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// MainWindow::s_about:
//
//
void
MainWindow::s_about()
{
    /** Slot function for Help|About
     */
    QMessageBox::about(this, "About MMP",
            "<center> Musically Musical Player </center> \
             <center> Lazy Asian Development Syndicate </center>");
}


// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// MainWindow::load_default_album_art:
//
//
void MainWindow::load_default_album_art(){
    /** Loads the default album art for use when art is missing.
     */
    m_default_album_art->load(":/art.png");
    m_labelArt->setPixmap(QPixmap::fromImage(m_default_album_art->scaled(200,200,Qt::KeepAspectRatio, Qt::SmoothTransformation)));
}


// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Extract album art
//
void MainWindow::s_extract_art(QTableWidgetItem* item){
    /** Slot function to extract album art, detects the file type of the music file, then creates an appropriate TagLib
    frame to extract the art.
    */
    bool valid = 0;
    TagLib::FileName fileName(m_table->item(item->row(),PATH)->text().toStdString().c_str()); //Gets the file name from the table widget
    TagLib::String fileTypefull = (m_table->item(item->row(),PATH)->text().toStdString().c_str()); //Converts the file name to a cstring
    TagLib::String fileType = fileTypefull.substr(fileTypefull.size() - 4).upper(); //Uses the last four charactersto detect the file type

        if (fileType == ".M4A" || fileType == ".MP4")
        {
            TagLib::MP4::File f(fileName);
            TagLib::MP4::Tag* tag = f.tag();
            TagLib::MP4::ItemListMap itemsListMap = tag->itemListMap();
            if (itemsListMap.isEmpty() == 0){
                m_curr_album_art = new QImage;
                valid = 1;
                TagLib::MP4::Item coverItem = itemsListMap["covr"];
                TagLib::MP4::CoverArtList coverArtList = coverItem.toCoverArtList();
                TagLib::MP4::CoverArt frame = coverArtList.front();
                m_curr_album_art->loadFromData((const uchar *) frame.data().data(),frame.data().size());
            }
        }
        else if (fileType == ".MP3")
        {
            TagLib::MPEG::File f(fileName);
            TagLib::ID3v2::Tag * mp3Tag;
            mp3Tag = f.ID3v2Tag();
            TagLib::ID3v2::FrameList l = mp3Tag->frameList("APIC");
            if (l.isEmpty() == 0){
                m_curr_album_art = new QImage;
                valid = 1;
                TagLib::ID3v2::AttachedPictureFrame *frame = static_cast<TagLib::ID3v2::AttachedPictureFrame *>(l.front());
                m_curr_album_art->loadFromData((const uchar *) frame->picture().data(), frame->picture().size());
            }
        }
         else if (fileType == "FLAC"){
             TagLib::FLAC::File f(fileName);
             const TagLib::List<TagLib::FLAC::Picture*>& picList = f.pictureList();
             if (picList.isEmpty() == 0) {
                 m_curr_album_art = new QImage;
                 valid = 1;
                 TagLib::FLAC::Picture* frame = picList[0];
                 m_curr_album_art->loadFromData((const uchar *) frame->data().data(), frame->data().size());
             }
        }

        if (valid == 0 || m_curr_album_art->isNull()){
              *m_curr_album_art = *m_default_album_art;
         }

          m_labelArt->setPixmap(QPixmap::fromImage(m_curr_album_art->scaled(200,200,Qt::KeepAspectRatio, Qt::SmoothTransformation)));
}


// Layout Actions
void MainWindow::s_toggleDocks(){
    /** Toggles the titlebar of all dock widgets when run.
     */
    if (!m_cdisabled){
        QWidget* EmptyWidget = new QWidget();
        coverflow_dock->setTitleBarWidget(EmptyWidget);

        QWidget* EmptyWidget2 = new QWidget();
        p_controls_dock->setTitleBarWidget(EmptyWidget2);

        QWidget* EmptyWidget3 = new QWidget();
        spectrum_dock->setTitleBarWidget(EmptyWidget3);

        QWidget* EmptyWidget4 = new QWidget();
        m_table_dock->setTitleBarWidget(EmptyWidget4);

        QWidget* EmptyWidget5 = new QWidget();
        m_panel_dock->setTitleBarWidget(EmptyWidget5);

        QWidget* EmptyWidget6 = new QWidget();
        m_playlist_dock->setTitleBarWidget(EmptyWidget6);

        QWidget* EmptyWidget7 = new QWidget();
        m_labelArt_dock->setTitleBarWidget(EmptyWidget7);

        m_cdisabled=1;
    }

    else {
        coverflow_dock->setTitleBarWidget( NULL );
        p_controls_dock->setTitleBarWidget( NULL );
        spectrum_dock->setTitleBarWidget( NULL );
        m_panel_dock->setTitleBarWidget( NULL );
        m_table_dock->setTitleBarWidget( NULL );
        m_playlist_dock->setTitleBarWidget( NULL );
        m_labelArt_dock->setTitleBarWidget( NULL );

        m_cdisabled = 0;

    }

}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Player slot functions
//
void MainWindow::s_play()
{
    /** Slot function to start playing music loaded into the musicplayer, also enables the progress slider
     */
    mediaplayer->play();
    m_progress_slider->setDisabled(false);
}

void MainWindow::s_pause()
{
    /** Slot function to pause the mediaplayer
     */
    mediaplayer->pause();
}

void MainWindow::s_stop()
{
    /** Slot function to stop the mediaplayer, also disables the progress slider
     */
    mediaplayer->stop();
    m_progress_slider->setDisabled(true);
}

void MainWindow::s_next()
{
    /** Slot function to skip to the next song
     */
    playlist->next();
    mediaplayer->play();
}

void MainWindow::s_previous()
{
    /** Slot funciton to go to the previous song
     */
    playlist->previous();
    mediaplayer->play();
}

void MainWindow::changeVolume(int volume){
    /** Slot function for the in player volume control
     */
    mediaplayer->setVolume(volume);
    if (mediaplayer->isMuted())
        s_mute(); //If you change the volume, turn the volume back on
}

void MainWindow::s_mute()
{
    /** Slot function to mute the player and set an appropriate icon for the mute button
     */
    if (mediaplayer->isMuted()==false){
        mediaplayer->setMuted(true);
        m_aud_controls[MUTE]->setIcon(QApplication::style()->standardIcon((QStyle::SP_MediaVolumeMuted)));
    }
    else {
        mediaplayer->setMuted(false);
        m_aud_controls[MUTE]->setIcon(QApplication::style()->standardIcon((QStyle::SP_MediaVolume)));
    }
}

void MainWindow::s_playmode(){
    /** Slot function to toggle shuffling and set an appropriate icon for the button
     */
    switch (playlist->playbackMode()){
    case (QMediaPlaylist::Sequential):
    playlist->setPlaybackMode(QMediaPlaylist::Random);
    player_controls[SHUFFLE]->setIcon(QApplication::style()->standardIcon(QStyle::SP_BrowserStop));
    break;

    case (QMediaPlaylist::Random):
    playlist->setPlaybackMode(QMediaPlaylist::Sequential);
    player_controls[SHUFFLE]->setIcon(QApplication::style()->standardIcon(QStyle::SP_BrowserReload));
    break;
    }
}

void MainWindow::s_ml_add_play(QTableWidgetItem *ml_item) {
    /** Slot function to add a song to the playlist, connect the slider and media player functions to their widgets, and push the album art to the coverflow widget */
    connect(mediaplayer, SIGNAL(stateChanged(QMediaPlayer::State)), this, SLOT(s_player_status(QMediaPlayer::State)));
    connect(mediaplayer, SIGNAL(durationChanged(qint64)), this, SLOT(s_duration(qint64)));
    connect(mediaplayer, SIGNAL(durationChanged(qint64)), this, SLOT(s_media_changed()));
    connect(mediaplayer, SIGNAL(positionChanged(qint64)), this, SLOT(playerStatusUpdate(qint64)));
    connect(mediaplayer, SIGNAL(positionChanged(qint64)), this, SLOT(s_progress_slider_loc_change(qint64)));
    connect(m_progress_slider, SIGNAL(sliderMoved(int)), this, SLOT(s_progress_slider_change(int)));


    int ml_row = ml_item->row();
    playlist->addMedia(QUrl::fromLocalFile(m_table->item(ml_row,PATH)->text().toStdString().c_str()));

    QTableWidgetItem *temp_item[2];
    m_playlist_view->insertRow(m_playlist_view->rowCount());

    int j = 2;
    for (int i = 1; i < 3; i++, j-=2){
        temp_item[i] = new QTableWidgetItem;
        temp_item[i]->setText(m_table->item(ml_row,j)->text());
        m_playlist_view->setItem(m_playlist_view->rowCount()-1, i, temp_item[i]);
    }

    //Plays the song as soon as it is added to the playlist
    s_play();

     //Link to coverflow
     coverflow->ArtList.push_back(*m_curr_album_art);
     coverflow->setNumberOfBoxes(coverflow->NumberOfBoxes()+1);

}

void MainWindow::s_playlist_spec(QTableWidgetItem* playlist_item){
    /** Slot function to play a song selected by the playlist */
    playlist->setCurrentIndex(playlist_item->row());
}

QString MainWindow::convertTime(qint64 time_in_sec) {
    /** Converts time from qint64 to QString for use in widgets */
    QString format = "mm:ss";
    if (time_in_sec > 3600)
        format = "hh:mm:ss";
    if (time_in_sec >= 0) {
        QTime fixTime((time_in_sec / 3600) % 60, (time_in_sec / 60) % 60,
            time_in_sec % 60, (time_in_sec * 1000) % 1000);
        return fixTime.toString(format);
    }
    else {
        return "--:--";
    }
}

void MainWindow::s_duration(qint64 duration){
    /** Slot function to set the range of the mediaplayer's seeker, using the duration from the song file */
    m_track_duration = (int)(duration / 1000);
    m_progress_slider->setRange(0,m_track_duration);
}

void MainWindow::playerStatusUpdate(qint64 media_position)
{
    /** Updates the player widgets */

    // Update timestring label
    qint64 currentInfo = media_position/1000;
    m_playtime->setText(convertTime(currentInfo) + " / " + convertTime(m_track_duration));

    // Update player slider
    m_progress_slider->setValue((int)currentInfo);
}


void MainWindow::s_progress_slider_loc_change(qint64 media_position)
{
    /** Slot function to update slider position */
    m_progress_slider->setValue((int)(media_position/1000));
}

void MainWindow::s_progress_slider_change(int slider_position)
{
    /** Slot function to change the position of the actual media player's position */
    qint64 slider_pos_64 = (qint64)slider_position;
    slider_pos_64 = slider_pos_64 * 1000;
    mediaplayer->setPosition(slider_pos_64);
}

void MainWindow::s_player_status(QMediaPlayer::State state)
{
    /** Slot function to change the status of the player buttons based on the mediaplayer's state, disable and enable the player buttons, and change the title of the program
    to the currently playing song, as well as connect to the spectrum widget */
    if (state == QMediaPlayer::StoppedState) {
        playerStatusUpdate(0);

        // Switch to play icon/function.
        player_controls[PLAY]->setIcon(QApplication::style()->standardIcon(QStyle::SP_MediaPlay));
        QObject::connect(player_controls[PLAY], SIGNAL(clicked()), this, SLOT(s_play()));
        m_progress_slider->setDisabled(true);
        QString title = "Musically Musical Player";
        setWindowTitle(title);

        m_playlist_view->setItem(m_last_playing, 0, NULL);
        spectrum_widget->stopFeed();
    }
    if (state == QMediaPlayer::PlayingState) {
        // Switch to pause icon/function.
        player_controls[PLAY]->setIcon(QApplication::style()->standardIcon(QStyle::SP_MediaPause));
        QObject::connect(player_controls[PLAY], SIGNAL(clicked()), this, SLOT(s_pause()));
        s_media_changed();

        QTableWidgetItem *icon = new QTableWidgetItem();
        QIcon actual(QApplication::style()->standardIcon(QStyle::SP_MediaPlay));
        icon->setIcon(actual);
        m_playlist_view->setItem(playlist->currentIndex(), 0, icon);
        spectrum_widget->startFeed();
    }
    if (state == QMediaPlayer::PausedState) {
        // Switch to play icon/function.
        player_controls[PLAY]->setIcon(QApplication::style()->standardIcon(QStyle::SP_MediaPlay));
        QObject::connect(player_controls[PLAY], SIGNAL(clicked()), this, SLOT(s_play()));
        QString title = mediaplayer->metaData(QMediaMetaData::AlbumArtist).toString() + " - [" + mediaplayer->metaData("AlbumTitle").toString() + "] " + mediaplayer->metaData("Title").toString() + " [PAUSED] ";
        setWindowTitle(title);

        QTableWidgetItem *icon = new QTableWidgetItem();
        QIcon actual(QApplication::style()->standardIcon(QStyle::SP_MediaPause));
        icon->setIcon(actual);
        m_playlist_view->setItem(playlist->currentIndex(), 0, icon);

        spectrum_widget->pauseFeed();
    }
}

void MainWindow::s_media_changed(){
    /** Slot function to place the play icon in the playlist, as well as update the title of the program to the now playing song */
    QString title = mediaplayer->metaData(QMediaMetaData::AlbumArtist).toString() + " - [" + mediaplayer->metaData("AlbumTitle").toString() + "] " + mediaplayer->metaData("Title").toString() + " [PLAYING] ";
    setWindowTitle(title);
    coverflow->move_to_index(playlist->currentIndex());
    QTableWidgetItem *icon = new QTableWidgetItem();
    QIcon actual(QApplication::style()->standardIcon(QStyle::SP_MediaPlay));
    m_playlist_view->setItem(m_last_playing, 0, NULL);
    icon->setIcon(actual);
    m_last_playing = playlist->currentIndex();
    m_playlist_view->setItem(m_last_playing,0, icon);
}

void MainWindow::s_playback_speed(int index){
    /** Slot function to change playback speed of the mediaplayer */
    switch (index){
    case 0:
        mediaplayer->setPlaybackRate(0.5);
        break;
    case 1:
        mediaplayer->setPlaybackRate(1.0);
        break;
    case 2:
        mediaplayer->setPlaybackRate(1.25);
        break;
    case 3:
        mediaplayer->setPlaybackRate(1.5);
        break;
    case 4:
        mediaplayer->setPlaybackRate(1.75);
        break;
    case 5:
        mediaplayer->setPlaybackRate(2.0);
        break;
    }
}
