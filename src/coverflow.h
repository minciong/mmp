#ifndef COVERFLOW_H
#define COVERFLOW_H

#include <QOpenGLWidget>
#include <QObject>
#include <QImage>
#include <QList>
#include <QTimer>

///////////////////////////////////////////////////////////////////////////////
///
/// \class Coverflow
/// \brief Coverflow class.
///
/// An album art visualizer that shows album art in a 3D flowing space.
///
///////////////////////////////////////////////////////////////////////////////


class Coverflow : public QOpenGLWidget
{
    Q_OBJECT

public:
    Coverflow(QWidget *parent = 0);
    void setNumberOfBoxes(int);
    int NumberOfBoxes();

    QList<QImage> ArtList;
    QList<GLuint> textures;

    void move_to_index(int);

public slots:
    void xplus();
    void xminus();
    void magic();

private:
        QTimer* m_timer;
        QTimer* a_timer;
        QTimer* b_timer;
        QTimer* magic_timer;

        int move_right = 0;
        int move_left = 0;

        double xloc = 0;
        bool leaving=0;
        double angle=0;
        int middleshift=0;
        int number_of_boxes = 0;
        int counter=0;
        bool allgood = 1;

        double speed=500.000;


        void loadTexture(int);
        void perspective(double fovy, double aspect, double zNear, double zFar);
        void drawbox(int);
        void keyPressEvent(QKeyEvent *event);

        void update();
        void display();

        void left_action_internal();
        void right_action_internal();

protected:
        void resizeGL(int width, int height);
        void paintGL();
        void initializeGL();

    };
#endif // COVERFLOW_H

